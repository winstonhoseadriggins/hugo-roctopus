+++
title = "License"
date = "2015-10-01"
+++

<div class="container">
  <div class="row">
    <div class="card">
      <div class="card-block">
        <h2 class="card-title">LICENSE</h2>
      </div>
      <div class="card-block">
        <p class="card-text">
          DW84 Inc LLC Foundation Universal Software Application
        </p>
      </div>
    </div>
  </div>
</div>
